<?php

/**
 * Declination
 */
function declOfNum($num, $titles) {
    $cases = array(2, 0, 1, 1, 1, 2);

    return $titles[($num % 100 > 4 && $num % 100 < 20) ? 2 : $cases[min($num % 10, 5)]];
}

/**
 * Getting an array of post IDs by metafield value
 */
function get_posts_ids_by_metas( $key, $value ){
	global $wpdb;
 
	// get an array of all IDs matching the given meta key and value
	$all_posts = $wpdb->get_col( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", $key, $value ) );
 
	return $all_posts;
}

/**
 * Saving a new field in the user's metadata
 */
add_action( 'user_register', 'myplugin_user_register' );
function myplugin_user_register( $user_id ) {
    if ( ! empty( $_POST['user_phone'] ) ) {
        update_user_meta( $user_id, 'user_phone', trim( $_POST['user_phone'] ) );
    }
}

/**
 * Getting a list of basic services
 */
function get_ids_main_services() {
    $services_array = [];

    $query = new WP_Query( array(
      'post_type' => array( 'home', 'office' ),
      'posts_per_page'   => -1,
      'meta_query' => array(
        'key' => 'id_erp',
        'type'    => 'NUMERIC',
        'compare' => 'EXISTS',
      )
    ) );
  
    if ($query->have_posts()) : 
      while ($query->have_posts()) : $query->the_post();
      $key_good = get_field('id_erp');
  
      if($key_good != "") $services_array[$key_good] = get_the_ID();
  
      endwhile;
    endif; 

    wp_reset_postdata();

    return $services_array;

    wp_die();
}

/**
 * Sort array by date
 */
function sort_by_date($a, $b) 
{
    if ($a["timestamp"] == $b["timestamp"]) {
        return 0;
    }
    return ($a["timestamp"] < $b["timestamp"]) ? -1 : 1;
}

/**
 * Sort array by delivery date
 */
function sort_by_date_deliverydate($a, $b) 
{
  $a = strtotime("-24 hours", strtotime($a["DeliveryDate"]));
  $b = strtotime("-24 hours", strtotime($b["DeliveryDate"]));

    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}

/**
 * Sort array by name
 */
function sort_by_name($a, $b)
{
    return ($a["Name"] < $b["Name"]) ? -1 : 1;
}

/**
 * Url check
 */
function is_url($url) {
  return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
}

/**
 * Print Success
 */
function outputSuccess($data = []) {
  $data['success'] = true;
  print_r(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
}

/**
 * Print Errore
 */
function outputError($code, $message, array $data = []) {
  $data['success'] = false;
  $data['errorCode'] = $code;
  $data['errorMessage'] = $message;
  print_r(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
}
