<?php

class Epos {

	const STATUS_OK = 'OK';
	const STATUS_ERROR = 'error';
	const ACCUOUNT = 'CLIENT_ACCOUNT';
	const API_KEY_FIRST = 'CLIENT_API_KEY';
	const API_KEY_SECOND = 'CLIENT_API_KEY_SECOND';
	const URL = 'API_URL';

    public $client;

	/**
	 *
	 * @var string
	 */
	private $errorCode = null;

	/**
	 *
	 * @var string
	 */
	private $errorMessage = null;
	
	/**
	 *
	 * @return string
	 */
	public function getErrorCode() {
		return $this->errorCode;
	}

	/**
	 *
	 * @param string $errorCode        	
	 * @return \SoloOrder\Service\Payment\Sberbank
	 */
	private function setErrorCode($errorCode) {
		$this->errorCode = $errorCode;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getErrorMessage() {
		return $this->errorMessage;
	}

	/**
	 *
	 * @param string $errorMessage        	
	 * @return \SoloOrder\Service\Payment\Sberbank
	 */
	private function setErrorMessage($errorMessage) {
		$this->errorMessage = $errorMessage;
		return $this;
	}


	/**
	 *Getting a signature to go to the payment page
	 * @param float $amount        	
	 * @param string $orderNumber        	
	 * @param int $paymentType        	
	 * @param string $backUrl        	
	 * @param string $description        	
	 * @return string
	 */
	public function getSignature($amount, $orderNumber, $description, $trtype, $paytoken = null, $backUrl = null) {

        $amountCurr = 'RUB';
		$currency = 'MBC';
        $account = self::ACCUOUNT;
        $key1 = self::API_KEY_1;
        $key2 = self::API_KEY_2;

		if(isset($paytoken) && $paytoken != "") {
			$parts = [
				$amount,
				$amountCurr,
				$currency,
				$orderNumber,
				$description,
				$trtype,
				$account,
				$paytoken,
				$backUrl,
				$key1,
				$key2
			];
		} else {
			$parts = [
				$amount,
				$amountCurr,
				$currency,
				$orderNumber,
				$description,
				$trtype,
				$account,
				$backUrl,
				$key1,
				$key2
			];
		}

		$signature = implode(':', $parts);
        $signature = strtoupper(md5($signature));

		return $signature;
	}


	/**
	 * receiving payment info from status page after payment
	 * @param string $orderId        	
	 * @return array
	 */
	public function getOrderInfoExtended($orderId, $transactionId) {
		$info = [];
		
		// signature
		$signatureParams = [];
		$signatureParams[] = 'check';
		$signatureParams[] = self::ACCUOUNT;
		$signatureParams[] = $transactionId;
		$signatureParams[] = self::API_KEY_1;
		$signatureParams[] = self::API_KEY_2;
		$signature = strtoupper(md5(implode(':', $signatureParams)));
		
		// post body
		$post = [];
		$post['opertype'] = 'check';
		$post['transID'] = $transactionId;
		$post['number'] = $orderId; 
		$post['account'] = self::ACCUOUNT;
		$post['appinfo '] = 0;
		$post['signature'] = $signature;
		
		$response = wp_remote_post( self::URL . 'payment/operate', array(
			'timeout'     => 5,
			'redirection' => 5,
			'httpversion' => '1.0',
			'blocking'    => true,
			'headers'     => array(),
			'body'        => $post, 
			'cookies'     => array()
		) );

		// проверка ошибки
		if ( is_wp_error( $response ) ) {
			return false;

		} else {
			$info = json_decode($response['body'], true);
			return $info;
		}
		return false;

	}

	/**
	 *getting the payment status itself
	 * @param string $orderId        	
	 * @return array
	 */
	public function getOrderStatus($orderId, $transactionId) {
		$info = [];
		
		// signature
		$signatureParams = [];
		$signatureParams[] = 'check';
		$signatureParams[] = self::ACCUOUNT;
		$signatureParams[] = $transactionId;
		$signatureParams[] = self::API_KEY_1;
		$signatureParams[] = self::API_KEY_2;
		$signature = strtoupper(md5(implode(':', $signatureParams)));
		
		// post body
		$post = [];
		$post['opertype'] = 'check';
		$post['transID'] = $transactionId;
		$post['number'] = $orderId; 
		$post['account'] = self::ACCUOUNT;
		$post['appinfo '] = 0;
		$post['signature'] = $signature;
		
		$response = wp_remote_post( self::URL . 'payment/operate', array(
			'timeout'     => 5,
			'redirection' => 5,
			'httpversion' => '1.0',
			'blocking'    => true,
			'headers'     => array(),
			'body'        => $post, 
			'cookies'     => array()
		) );

		if ( is_wp_error( $response ) ) {
			return false;
		} else {
			$info = json_decode($response['body'], true);
			if (isset($info['status'])) {
				return $info['status'];
			}
		}

		return false;
	}

	/**
	 * Deposit
	 * @param integer $orderId        	
	 * @param float $amount        	
	 * @return integer
	 */
	public function deposit($transactionId, $amount) {
		$result = false;
		
		$opertype = 'terminate';
		
		// signature
		$signatureParams = [];
		$signatureParams[] = $opertype;
		$signatureParams[] = $amount;
		$signatureParams[] = self::ACCUOUNT;
		$signatureParams[] = $transactionId;
		$signatureParams[] = self::API_KEY_1;
		$signatureParams[] = self::API_KEY_2;
		$signature = strtoupper(md5(implode(':', $signatureParams)));
		
		// post body
		$post = [];
		$post['opertype'] = $opertype;
		$post['amountterminate'] = $amount;
		$post['account'] = self::ACCUOUNT;
		$post['transID'] = $transactionId;
		$post['signature'] = $signature;
		
		// response
		$response = wp_remote_post( self::URL . 'payment/operate', array(
			'timeout'     => 5,
			'redirection' => 5,
			'httpversion' => '1.0',
			'blocking'    => true,
			'headers'     => array(),
			'body'        => $post, // параметры запроса в массиве
			'cookies'     => array()
		) );

		if ( is_wp_error( $response ) ) {
			return false;
		} else {
			$info = json_decode($response['body'], true);
			if (self::STATUS_OK == $info['status']) {
				$result = true;
			} elseif (self::STATUS_ERROR == $info['status']) {
				$this->processError($info);
			}
		}

		return $result;
	}

	/**
	 * creating a payment for an order  	
	 * @param string $orderNumber        	
	 * @param float $amount        	
	 * @param string $returnUrl        	
	 * @return boolean
	 */
	public function registerOrder($orderNumber, $amount, $transactionId, $recurringId) {
		$result = false;
		
		$opertype = 'recurring';
		
		// signature
		$signatureParams = [];
		$signatureParams[] = $opertype;
		$signatureParams[] = $amount;
		$signatureParams[] = self::ACCUOUNT;
		$signatureParams[] = $transactionId;
		$signatureParams[] = $recurringId;
		$signatureParams[] = $orderNumber;
		$signatureParams[] = self::API_KEY_1;
		$signatureParams[] = self::API_KEY_2;
		$signature = strtoupper(md5(implode(':', $signatureParams)));

		// post body
		$post = [];
		$post['opertype'] = $opertype;
		$post['amountrecurring'] = $amount;
		$post['account'] = self::ACCUOUNT;
		$post['transIDparent'] = $transactionId;
		$post['recurringID'] = $recurringId;
		$post['numberrecurring'] = $orderNumber;
		$post['signature'] = $signature;

		// response
		$response = wp_remote_post( self::URL . 'payment/operate', array(
			'timeout'     => 5,
			'redirection' => 5,
			'httpversion' => '1.0',
			'blocking'    => true,
			'headers'     => array(),
			'body'        => $post, 
			'cookies'     => array()
		) );

		if ( is_wp_error( $response ) ) {
			return false;
		} else {
			$responseData = json_decode($response['body'], true);
			return $responseData;
		}

		return false;
	}


	/**
	 * creating a payment for an order 	
	 * @param string $orderNumber        	
	 * @param float $amount        	
	 * @param string $returnUrl        	
	 * @return boolean
	 */
	public function registerPreAuthOrder($orderNumber, $amount, $transactionId, $recurringId) {

		$result = false;
		
		$opertype = 'recurring';
		$trtype = 2;

		// signature
		$signatureParams = [];
		$signatureParams[] = $opertype;
		$signatureParams[] = $amount;
		$signatureParams[] = self::ACCUOUNT;
		$signatureParams[] = $transactionId;
		$signatureParams[] = $recurringId;
		$signatureParams[] = $orderNumber;
		$signatureParams[] = $trtype;
		$signatureParams[] = self::API_KEY_1;
		$signatureParams[] = self::API_KEY_2;
		$signature = strtoupper(md5(implode(':', $signatureParams)));

		// post body
		$post = [];
		$post['opertype'] = $opertype;
		$post['amountrecurring'] = $amount;
		$post['trtype'] = $trtype;
		$post['account'] = self::ACCUOUNT;
		$post['transIDparent'] = $transactionId;
		$post['recurringID'] = $recurringId;
		$post['numberrecurring'] = $orderNumber;
		$post['signature'] = $signature;		

		// response
		$response = wp_remote_post( self::URL . 'payment/operate', array(
			'timeout'     => 5,
			'redirection' => 5,
			'httpversion' => '1.0',
			'blocking'    => true,
			'headers'     => array(),
			'body'        => $post, 
			'cookies'     => array()
		) );

		if ( is_wp_error( $response ) ) {
			return false;
		} else {
			$responseData = json_decode($response['body'], true);
			return $responseData;
		}

		return false;
	}

	/**
	 * release of funds
	 * @param integer $orderId        	
	 * @return integer
	 */
	public function reverse($orderId) {
		$result = false;
		
		// signature
		$signatureParams = [];
		$signatureParams[] = 'unblock';
		$signatureParams[] = self::ACCUOUNT;
		$signatureParams[] = $orderId;
		$signatureParams[] = self::API_KEY_1;
		$signatureParams[] = self::API_KEY_2;
		$signature = strtoupper(md5(implode(':', $signatureParams)));
		
		// post body
		$post = [];
		$post['opertype'] = 'unblock';
		$post['transID'] = $orderId;
		$post['account'] = self::ACCUOUNT;
		$post['signature'] = $signature;

		// response
		$response = wp_remote_post( self::URL . 'payment/operate', array(
			'timeout'     => 5,
			'redirection' => 5,
			'httpversion' => '1.0',
			'blocking'    => true,
			'headers'     => array(),
			'body'        => $post, 
			'cookies'     => array()
		) );

		if (! is_wp_error( $response ) ) {
			$data = json_decode($response['body'], true);

			$result = (isset($data['status']) && (self::STATUS_OK == $data['status']));
			if (!$result) {
				$this->processError($data);
			}
		}

		return $result;
	}

	/**
	 * error output
	 * @param array $response        	
	 */
	private function processError($response) {
		$this->errorCode = null;
		$this->errorMessage = null;
		if (isset($response['errorcode'])) $this->errorCode = $response['errorcode'];
		if (isset($response['errortext'])) $this->errorMessage = $response['errortext'];
	}

	/**
	 * error checking
	 * @return boolean
	 */
	public function hasError() {
		return ((null !== $this->errorCode) && (0 != $this->errorCode));
	}

}